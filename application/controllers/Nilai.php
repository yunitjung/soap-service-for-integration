<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nilai extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Nilai", "urn:Nilai");

      //req ips array
      $this->nusoap_server->wsdl->addComplexType(
        'req_ips',
        'element',
        'struct',
        'all',
        '',
        array(
            'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
            'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
            'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
            'semester'       => array('name' => 'semester', 'type' => 'xsd:string'),
            'jml_kompen'      => array('name' => 'jml_kompen', 'type' => 'xsd:string'),
            'rata_ips'      => array('name' => 'rata_ips', 'type' => 'xsd:string')
        )
    );

    //list of req ips
    $this->nusoap_server->wsdl->addComplexType(
        'list_ips',
        'complexType', 'array', '', 'SOAP-ENC:Array',
        array(
            'req_ips' => array('name' => 'req_ips', 'type' => 'tns:req_ips')),
        array(
            'req_ips' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_ips[]')),
        'tns:req_ips'
        );

    //return ft ips
    $this->nusoap_server->wsdl->addComplexType(
        'ft_ips',
        'complexType',
        'struct',
        'all',
        '',
        array(
            'id_dim_angkatan' => array(
                'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
            ),
            'id_dim_prodi' => array(
                'name' => 'id_dim_prodi', 'type' => 'xsd:int'
            ),
            'id_dim_semester' => array(
                'name' => 'id_dim_semester', 'type' => 'xsd:string'
            ),
            'id_dim_jeniskelamin' => array(
                'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
            ),
            'jml_kompen' => array(
                'name' => 'jml_kompen', 'type' => 'xsd:int'
            ),
            'rata_ips' => array(
                'name' => 'rata_ips', 'type' => 'xsd:double'
            )

        )
    );

    //list of ft ips
    $this->nusoap_server->wsdl->addComplexType(
        'daftar_ft_ips',
        'complexType', 'array', '', 'SOAP-ENC:Array',
        array(
            'ft_ips' => array('name' => 'ft_ips', 'type' => 'tns:ft_ips')),
        array(
            'ft_ips' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_ips[]')),
        'tns:ft_ips'
    );

    //fact table ips
    $this->nusoap_server->wsdl->addComplexType(
        'fact_table_ips',
        'complexType',
        'struct',
        'all',
        '',
        array(
            'jenis_dt' => array(
                'name' => 'jenis_dt', 'type' => 'xsd:string'
            ),
            'daftar_ft_ips' => array(
                'name' => 'daftar_ft_ips', 'type' => 'tns:daftar_ft_ips'
            )
        )
    );

     //registering create ft ips
     $this->nusoap_server->register(
        "create_ft_ips",
        array("data" => 'tns:list_ips'),
        array("return" => 'tns:fact_table_ips'),
        "urn:Service1",
        "urn:Service1#create_ft_ips",
        "rpc",
        "encoded",
        "Returning ft ips"
    );


    }

    function index() {

        function create_ft_ips($list_ips)
        {
            $CI =& get_instance();
            $daftar_ft_ips = array();
            $daftar_ft_ips['jenis_dt'] =1;
            foreach($list_ips as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['id_dim_semester'] = $l['semester'];
                $tmp_arr['jml_kompen'] = $l['jml_kompen'];
                $tmp_arr['rata_ips'] = $l['rata_ips'];

                $daftar_ft_ips['daftar_ft_ips'][]= $tmp_arr;


            }
            return $daftar_ft_ips;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}