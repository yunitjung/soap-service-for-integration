<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Prodi extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Prodi", "urn:Prodi");

        //req prodi array
        $this->nusoap_server->wsdl->addComplexType(
            'req_prodi',
            'complexType',
            'array',
            'req_prodi',
            '',
            array(
                'prodi_id'         => array('name' => 'prodi_id', 'type' => 'xsd:int'),
                'nama_prodi'       => array('name' => 'nama_prodi', 'type' => 'xsd:string'),
                'akreditasi'       => array('name' => 'akreditasi', 'type' => 'xsd:string'),
                'status'           => array('name' => 'status', 'type' => 'xsd:string'),
                'konsentrasi'      => array('name' => 'konsentrasi', 'type' => 'xsd:string')
            )
        );
        //list of req prodi
        $this->nusoap_server->wsdl->addComplexType(
            'list_prodi',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_prodi' => array('name' => 'req_prodi', 'type' => 'tns:req_prodi')),
            array(
                'req_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_prodi[]')),
            'tns:req_prodi'
        );

        //array for dim_prodi
        $this->nusoap_server->wsdl->addComplexType(
            'dim_prodi',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_prodi'  => array(
                    'name'  => 'id_dim_prodi', 'type' => 'xsd:int',
                ),
                'nama_prodi' => array(
                    'name' => 'nama_prodi', 'type' => 'xsd:string'
                )
            )
        );

        //daftar of dim prodi
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_prodi',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_prodi' => array('name' => 'dim_prodi', 'type' => 'tns:dim_prodi')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_prodi[]')),
            'tns:dim_prodi'
        );

        //list of dim prodi with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_prodi',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_prodi' => array(
                    'name' => 'daftar_dim_prodi', 'type' => 'tns:daftar_dim_prodi'
                )
            )
        );

        //registering create_dim_prodi
        $this->nusoap_server->register(
            "create_dim_prodi",
            array("list_prodi" => "tns:list_prodi"),
            array("return" => "tns:list_dim_prodi"),
            "urn:Service1",
            "urn:Service1#create_dim_prodi",
            "rpc",
            "encoded",
            "Returning dim prodi"
        );
        /*end prodi*/
    }

    function index() {

        function create_dim_prodi($list_prodi)
        {
            $data_to_return = array();
            foreach($list_prodi as $l)
            {
                $tmp_arr = array(
                    'id_dim_prodi' => $l['prodi_id'],
                    'nama_prodi' => $l['nama_prodi']
                );
                $data_to_return['daftar_dim_prodi'][] = $tmp_arr;
            }
            return $data_to_return;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}