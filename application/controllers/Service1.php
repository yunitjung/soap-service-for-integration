<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service1 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dimensisave_model', 'dim'); //load your models here
        $this->load->model('facttablesave_model', 'm');
        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Service1", "urn:Service1");

         /*angkatan*/
        //req angkatan array
        $this->nusoap_server->wsdl->addComplexType(
            'req_angkatan',
            'complexType',
            'array',
            'req_angkatan',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string')
            )
        );
        //list of req angkatan
        $this->nusoap_server->wsdl->addComplexType(
            'list_angkatan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_angkatan' => array('name' => 'req_angkatan', 'type' => 'tns:req_angkatan')),
            array(
                'req_angkatan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_angkatan[]')),
            'tns:req_angkatan'
        );

        //array for dim_angkatan
        $this->nusoap_server->wsdl->addComplexType(
            'dim_angkatan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan'  => array(
                    'name'  => 'id_dim_angkatan', 'type' => 'xsd:int',
                ),
                'angkatan' => array(
                    'name' => 'angkatan', 'type' => 'xsd:int'
                )
            )
        );

        //daftar of dim angkatan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_angkatan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_angkatan' => array('name' => 'dim_angkatan', 'type' => 'tns:dim_angkatan')),
            array(
                'dim_angkatan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_angkatan[]')),
            'tns:dim_angkatan'
        );

        //list of dim angkatan with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_angkatan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_angkatan' => array(
                    'name' => 'daftar_dim_angkatan', 'type' => 'tns:daftar_dim_angkatan'
                )
            )
        );

        //registering create_dim_angkatan
        $this->nusoap_server->register(
            "create_dim_angkatan",
            array("list_angkatan" => "tns:list_angkatan"),
            array("return" => "tns:list_dim_angkatan"),
            "urn:Service1",
            "urn:Service1#create_dim_angkatan",
            "rpc",
            "encoded",
            "Returning dim angkatan"
        );
        /*end angkatan*/

        /*prodi*/
        //req prodi array
        $this->nusoap_server->wsdl->addComplexType(
            'req_prodi',
            'complexType',
            'array',
            'req_prodi',
            '',
            array(
                'prodi_id'         => array('name' => 'prodi_id', 'type' => 'xsd:int'),
                'nama_prodi'       => array('name' => 'nama_prodi', 'type' => 'xsd:string'),
                'akreditasi'       => array('name' => 'akreditasi', 'type' => 'xsd:string'),
                'status'           => array('name' => 'status', 'type' => 'xsd:string'),
                'konsentrasi'      => array('name' => 'konsentrasi', 'type' => 'xsd:string')
            )
        );
        //list of req prodi
        $this->nusoap_server->wsdl->addComplexType(
            'list_prodi',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_prodi' => array('name' => 'req_prodi', 'type' => 'tns:req_prodi')),
            array(
                'req_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_prodi[]')),
            'tns:req_prodi'
        );

        //array for dim_prodi
        $this->nusoap_server->wsdl->addComplexType(
            'dim_prodi',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_prodi'  => array(
                    'name'  => 'id_dim_prodi', 'type' => 'xsd:int',
                ),
                'nama_prodi' => array(
                    'name' => 'nama_prodi', 'type' => 'xsd:string'
                )
            )
        );

        //daftar of dim prodi
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_prodi',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_prodi' => array('name' => 'dim_prodi', 'type' => 'tns:dim_prodi')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_prodi[]')),
            'tns:dim_prodi'
        );

        //list of dim prodi with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_prodi',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_prodi' => array(
                    'name' => 'daftar_dim_prodi', 'type' => 'tns:daftar_dim_prodi'
                )
            )
        );

        //registering create_dim_prodi
        $this->nusoap_server->register(
            "create_dim_prodi",
            array("list_prodi" => "tns:list_prodi"),
            array("return" => "tns:list_dim_prodi"),
            "urn:Service1",
            "urn:Service1#create_dim_prodi",
            "rpc",
            "encoded",
            "Returning dim prodi"
        );
        /*end prodi*/


        /*bidangkompetensi*/
        //req bidang kompetensi
        $this->nusoap_server->wsdl->addComplexType(
            'req_bk',
            'complexType',
            'array',
            'all',
            '',
            array(
                'id_dim_bidangkompetensi'=> array('name' => 'id_dim_bidangkompetensi', 'type' => 'xsd:int'),
                'bidangkompetensi'       => array('name' => 'bidangkompetensi', 'type' => 'xsd:string'),
                'status'                 => array('name' => 'status', 'type' => 'xsd:string')
            )
        );

        //list of bk
        $this->nusoap_server->wsdl->addComplexType(
        'list_bk',
        'complexType', 'array', '', 'SOAP-ENC:Array',
        array(
            'req_bk' => array('name' => 'req_bk', 'type' => 'tns:req_bk')),
        array(
            'req_bk' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_bk[]')),
        'tns:req_bk'
        );

        //array for dim_bk
        $this->nusoap_server->wsdl->addComplexType(
            'dim_bk',
            'complexType',
            'struct',
            'all',
            '',
            array(
                    'id_dim_bidangkompetensi' => array(
                        'name' => 'id_dim_bidangkompetensi', 'type' => 'xsd:int'
                    ),
                    'bidangkompetensi' => array(
                        'name' => 'bidangkompetensi', 'type' => 'xsd:string')
            )
        );

        //daftar of dim bk
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_bk',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_bk' => array('name' => 'dim_bk', 'type' => 'tns:dim_bk')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_bk[]')),
            'tns:dim_bk'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_bk',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_bk' => array(
                    'name' => 'daftar_dim_bk', 'type' => 'tns:daftar_dim_bk'
                )
            )
        );

        //registering create_dim_bk
        $this->nusoap_server->register(
            "create_dim_bk",
            array("data" => 'tns:list_bk'),
            array("return" => 'tns:list_dim_bk'),
            "urn:Service1",
            "urn:Service1#create_dim_bk",
            "rpc",
            "encoded",
            "Returning dim bk"
        );


        //req jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'req_jlulusan',
            'complexType',
            'array',
            'all',
            '',
            array(
                'id_dim_jlulusan'        => array('name' => 'id_dim_jlulusan', 'type' => 'xsd:int'),
                'status'                 => array('name' => 'status', 'type' => 'xsd:string')
            )
        );

        //list of jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'list_jlulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_jlulusan' => array('name' => 'req_jlulusan', 'type' => 'tns:req_jlulusan')),
            array(
                'req_jlulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_jlulusan[]')),
            'tns:req_jlulusan'
            );

        //array for dim_jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'dim_jlulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                    'id_dim_jlulusan' => array(
                        'name' => 'id_dim_jlulusan', 'type' => 'xsd:int'
                    ),
                    'jenis_lulusan' => array(
                        'name' => 'jenis_lulusan', 'type' => 'xsd:string')
            )
        );

        //daftar of dim_jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_jlulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_jlulusan' => array('name' => 'dim_jlulusan', 'type' => 'tns:dim_jlulusan')),
            array(
                'dim_jlulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_jlulusan[]')),
            'tns:dim_jlulusan'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_jlulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_jlulusan' => array(
                    'name' => 'daftar_dim_jlulusan', 'type' => 'tns:daftar_dim_jlulusan'
                )
            )
        );

        //registering create_dim_jlulusan
        $this->nusoap_server->register(
            "create_dim_jlulusan",
            array("data" => 'tns:list_jlulusan'),
            array("return" => 'tns:list_dim_jlulusan'),
            "urn:Service1",
            "urn:Service1#create_dim_jlulusan",
            "rpc",
            "encoded",
            "Returning dim bk"
        );

        //req jenissekolah
        $this->nusoap_server->wsdl->addComplexType(
            'req_jenissekolah',
            'complexType',
            'array',
            'all',
            '',
            array(
                'id_dim_jenissekolah' => array(
                    'name' => 'id_dim_jenissekolah', 'type' => 'xsd:int'
                ),
                'jenissekolah' => array('name' => 'jenissekolah', 'type' => 'xsd:string'),
            )
        );

        //list of jenissekolah
        $this->nusoap_server->wsdl->addComplexType(
            'list_jenissekolah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_jenissekolah' => array('name' => 'req_jenissekolah', 'type' => 'tns:req_jenissekolah')),
            array(
                'req_jenissekolah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_jenissekolah[]')),
            'tns:req_jenissekolah'
            );

         //array for dim_jenissekolah
         $this->nusoap_server->wsdl->addComplexType(
            'dim_jenissekolah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                    'id_dim_jenissekolah' => array(
                        'name' => 'id_dim_jenissekolah', 'type' => 'xsd:int'
                    ),
                    'jenissekolah' => array(
                        'name' => 'jenissekolah', 'type' => 'xsd:string')
            )
        );

         //daftar of dim_jenissekolah
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_jenissekolah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_jenissekolah' => array('name' => 'dim_jenissekolah', 'type' => 'tns:dim_jenissekolah')),
            array(
                'dim_jenissekolah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_jenissekolah[]')),
            'tns:dim_jenissekolah'
        );

        //list of dim_jenissekolah with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_jenissekolah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_jenissekolah' => array(
                    'name' => 'daftar_dim_jenissekolah', 'type' => 'tns:daftar_dim_jenissekolah'
                )
            )
        );


        //registering create_dim_jenissekolah
        $this->nusoap_server->register(
            "create_dim_jenissekolah",
            array("data" => 'tns:list_jenissekolah'),
            array("return" => 'tns:list_dim_jenissekolah'),
            "urn:Service1",
            "urn:Service1#create_dim_jenissekolah",
            "rpc",
            "encoded",
            "Returning dim jenissekolah"
        );

        //req subjalur
        $this->nusoap_server->wsdl->addComplexType(
            'req_subjalur',
            'complexType',
            'array',
            'all',
            '',
            array(
                'subjalur_id' => array('name' => 'subjalur_id', 'type' => 'xsd:int'),
                'subjalur' => array('name' => 'subjalur', 'type' => 'xsd:string'),
                'jalur_id' => array('name' => 'jalur_id', 'type' => 'xsd:string')
            )
        );

         //list of subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'list_subjalur',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_subjalur' => array('name' => 'req_subjalur', 'type' => 'tns:req_subjalur')),
            array(
                'req_subjalur' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_subjalur[]')),
            'tns:req_subjalur'
        );

         //array for dim_subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'dim_subjalur',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_subjalur' => array(
                    'name' => 'id_dim_subjalur', 'type' => 'xsd:int'
                ),
                'subjalur' => array(
                    'name' => 'subjalur', 'type' => 'xsd:string'
                ),
                'id_dim_jalurmasuk' => array(
                    'name' => 'id_dim_jalurmasuk', 'type' => 'xsd:string'
                )
            )
        );

         //daftar of dim_subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_subjalur',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_subjalur' => array('name' => 'dim_subjalur', 'type' => 'tns:dim_subjalur')),
            array(
                'dim_subjalur' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_subjalur[]')),
            'tns:dim_subjalur'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_subjalur',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_subjalur' => array(
                    'name' => 'daftar_dim_subjalur', 'type' => 'tns:daftar_dim_subjalur'
                )
            )
        );


         //registering create_dim_subjalur
         $this->nusoap_server->register(
            "create_dim_subjalur",
            array("data" => 'tns:list_subjalur'),
            array("return" => 'tns:list_dim_subjalur'),
            "urn:Service1",
            "urn:Service1#create_dim_subjalur",
            "rpc",
            "encoded",
            "Returning dim subjalur"
        );


        //req asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'req_asaldaerah',
            'complexType',
            'array',
            'all',
            '',
            array(
                'asaldaerah_id' => array('name' => 'asaldaerah_id', 'type' => 'xsd:string'),
                'kota_siswa' => array('name' => 'kota_siswa', 'type' => 'xsd:string'),
                'propinsi_siswa' => array('name' => 'propinsi_siswa', 'type' => 'xsd:string')
            )
        );

        //list of asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'list_asaldaerah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_asaldaerah' => array('name' => 'req_asaldaerah', 'type' => 'tns:req_asaldaerah')),
            array(
                'req_asaldaerah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_asaldaerah[]')),
            'tns:req_asaldaerah'
            );

         //array for dim_asaldaerah
         $this->nusoap_server->wsdl->addComplexType(
            'dim_asaldaerah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_asaldaerah' => array('name' => 'id_dim_asaldaerah', 'type' => 'xsd:string'),
                'kota' => array(
                    'name' => 'kota', 'type' => 'xsd:string'
                ),
                'propinsi' => array(
                    'name' => 'propinsi', 'type' => 'xsd:string'
                )
            )
        );

        //daftar of dim_asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_asaldaerah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_asaldaerah' => array('name' => 'dim_asaldaerah', 'type' => 'tns:dim_asaldaerah')),
            array(
                'dim_asaldaerah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_asaldaerah[]')),
            'tns:dim_asaldaerah'
        );

        //list of dim_asaldaerah with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_asaldaerah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_asaldaerah' => array(
                    'name' => 'daftar_dim_asaldaerah', 'type' => 'tns:daftar_dim_asaldaerah'
                )
            )
        );

        //registering create_dim_asaldaerah
        $this->nusoap_server->register(
            "create_dim_asaldaerah",
            array("data" => 'tns:list_asaldaerah'),
            array("return" => 'tns:list_dim_asaldaerah'),
            "urn:Service1",
            "urn:Service1#create_dim_asaldaerah",
            "rpc",
            "encoded",
            "Returning dim asaldaerah"
        );

        //req kelulusan array
        $this->nusoap_server->wsdl->addComplexType(
            'req_kelulusan',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'bidangkompetensi_id' => array('name' => 'bidangkompetensi_id', 'type' => 'xsd:string'),
                'status'      => array('name' => 'status', 'type' => 'xsd:string'),
                'jml_lulusan'      => array('name' => 'jml_lulusan', 'type' => 'xsd:string'),
                'rata_ipk'      => array('name' => 'rata_ipk', 'type' => 'xsd:string')
            )
        );

        //list of req kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'list_kelulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_kelulusan' => array('name' => 'req_kelulusan', 'type' => 'tns:req_kelulusan')),
            array(
                'req_kelulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_kelulusan[]')),
            'tns:req_kelulusan'
            );

        //return ft kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'ft_kelulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_bidangkompetensi' => array(
                    'name' => 'id_dim_bidangkompetensi', 'type' => 'xsd:string'
                ),
                'id_dim_jlulusan' => array(
                    'name' => 'id_dim_jlulusan', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'rata_ipk' => array(
                    'name' => 'rata_ipk', 'type' => 'xsd:int'
                ),
                'jml_lulusan' => array(
                    'name' => 'jml_lulusan', 'type' => 'xsd:double'
                )

            )
        );

        //list of ft kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_kelulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_kelulusan' => array('name' => 'ft_kelulusan', 'type' => 'tns:ft_kelulusan')),
            array(
                'ft_kelulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_kelulusan[]')),
            'tns:ft_kelulusan'
        );

        //fact table kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_kelulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_kelulusan' => array(
                    'name' => 'daftar_ft_kelulusan', 'type' => 'tns:daftar_ft_kelulusan'
                )
            )
        );

         //registering create ft kelulusan
         $this->nusoap_server->register(
            "create_ft_kelulusan",
            array("data" => 'tns:list_kelulusan'),
            array("return" => 'tns:fact_table_kelulusan'),
            "urn:Service1",
            "urn:Service1#create_ft_kelulusan",
            "rpc",
            "encoded",
            "Returning ft kelulusan"
        );

          //req ips array
          $this->nusoap_server->wsdl->addComplexType(
            'req_ips',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'semester'       => array('name' => 'semester', 'type' => 'xsd:string'),
                'jml_kompen'      => array('name' => 'jml_kompen', 'type' => 'xsd:string'),
                'rata_ips'      => array('name' => 'rata_ips', 'type' => 'xsd:string')
            )
        );

        //list of req ips
        $this->nusoap_server->wsdl->addComplexType(
            'list_ips',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_ips' => array('name' => 'req_ips', 'type' => 'tns:req_ips')),
            array(
                'req_ips' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_ips[]')),
            'tns:req_ips'
            );

        //return ft ips
        $this->nusoap_server->wsdl->addComplexType(
            'ft_ips',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_semester' => array(
                    'name' => 'id_dim_semester', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'jml_kompen' => array(
                    'name' => 'jml_kompen', 'type' => 'xsd:int'
                ),
                'rata_ips' => array(
                    'name' => 'rata_ips', 'type' => 'xsd:double'
                )

            )
        );

        //list of ft ips
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_ips',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_ips' => array('name' => 'ft_ips', 'type' => 'tns:ft_ips')),
            array(
                'ft_ips' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_ips[]')),
            'tns:ft_ips'
        );

        //fact table ips
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_ips',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_ips' => array(
                    'name' => 'daftar_ft_ips', 'type' => 'tns:daftar_ft_ips'
                )
            )
        );

         //registering create ft ips
         $this->nusoap_server->register(
            "create_ft_ips",
            array("data" => 'tns:list_ips'),
            array("return" => 'tns:fact_table_ips'),
            "urn:Service1",
            "urn:Service1#create_ft_ips",
            "rpc",
            "encoded",
            "Returning ft ips"
        );

          //req asalmhs array
          $this->nusoap_server->wsdl->addComplexType(
            'req_asalmhs',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'asaldaerah_id'           => array('name' => 'asaldaerah_id', 'type' => 'xsd:string'),
                'subjalur_id'           => array('name' => 'subjalur_id', 'type' => 'xsd:string'),
                'jenissekolah_id'           => array('name' => 'jenissekolah_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'jml_pendaftar'      => array('name' => 'jml_pendaftar', 'type' => 'xsd:string'),
                'jml_kotaasal'      => array('name' => 'jml_kotaasal', 'type' => 'xsd:string'),
                'jml_subjalur'      => array('name' => 'jml_subjalur', 'type' => 'xsd:string'),
                'jml_jenissekolah'      => array('name' => 'jml_jenissekolah', 'type' => 'xsd:string')

            )
        );

        //list of req asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'list_asalmhs',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_asalmhs' => array('name' => 'req_asalmhs', 'type' => 'tns:req_asalmhs')),
            array(
                'req_asalmhs' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_asalmhs[]')),
            'tns:req_asalmhs'
            );

        //return ft asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'ft_asalmhs',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_asaldaerah' => array(
                    'name' => 'id_dim_asaldaerah', 'type' => 'xsd:string'
                ),
                'id_dim_subjalur' => array(
                    'name' => 'id_dim_subjalur', 'type' => 'xsd:string'
                ),
                'id_dim_jenissekolah' => array(
                    'name' => 'id_dim_jenissekolah', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'jml_pendaftar' => array(
                    'name' => 'jml_pendaftar', 'type' => 'xsd:int'
                ),
                'jml_kotaasal' => array(
                    'name' => 'jml_kotaasal', 'type' => 'xsd:int'
                ),
                'jml_subjalur' => array(
                    'name' => 'jml_subjalur', 'type' => 'xsd:int'
                ),
                'jml_jenissekolah' => array(
                    'name' => 'jml_jenissekolah', 'type' => 'xsd:int'
                )
            )
        );

        //list of ft asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_asalmhs',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_asalmhs' => array('name' => 'ft_asalmhs', 'type' => 'tns:ft_asalmhs')),
            array(
                'ft_asalmhs' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_asalmhs[]')),
            'tns:ft_asalmhs'
        );

        //fact table asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_asalmhs',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_asalmhs' => array(
                    'name' => 'daftar_ft_asalmhs', 'type' => 'tns:daftar_ft_asalmhs'
                )
            )
        );

         //registering create ft asalmhs
         $this->nusoap_server->register(
            "create_ft_asalmhs",
            array("data" => 'tns:list_asalmhs'),
            array("return" => 'tns:fact_table_asalmhs'),
            "urn:Service1",
            "urn:Service1#create_ft_asalmhs",
            "rpc",
            "encoded",
            "Returning ft asalmhs"
        );

        //req asalkinerja array
        $this->nusoap_server->wsdl->addComplexType(
            'req_asalkinerja',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'asaldaerah_id'           => array('name' => 'asaldaerah_id', 'type' => 'xsd:string'),
                'subjalur_id'           => array('name' => 'subjalur_id', 'type' => 'xsd:string'),
                'jenissekolah_id'           => array('name' => 'jenissekolah_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'rata_ipk'      => array('name' => 'rata_ipk', 'type' => 'xsd:string'),
                'jml_kompen'      => array('name' => 'jml_kompen', 'type' => 'xsd:string')
            )
        );

        //list of req asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'list_asalkinerja',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_asalkinerja' => array('name' => 'req_asalkinerja', 'type' => 'tns:req_asalkinerja')),
            array(
                'req_asalkinerja' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_asalkinerja[]')),
            'tns:req_asalkinerja'
            );

        //return ft asalkinerja
        $this->nusoap_server->wsdl->addComplexType(
            'ft_asalkinerja',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_asaldaerah' => array(
                    'name' => 'id_dim_asaldaerah', 'type' => 'xsd:string'
                ),
                'id_dim_subjalur' => array(
                    'name' => 'id_dim_subjalur', 'type' => 'xsd:string'
                ),
                'id_dim_jenissekolah' => array(
                    'name' => 'id_dim_jenissekolah', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'rata_ipk' => array(
                    'name' => 'rata_ipk', 'type' => 'xsd:double'
                ),
                'jml_kompen' => array(
                    'name' => 'jml_kompen', 'type' => 'xsd:int'
                )
            )
        );

        //list of ft asalkinerja
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_asalkinerja',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_asalkinerja' => array('name' => 'ft_asalkinerja', 'type' => 'tns:ft_asalkinerja')),
            array(
                'ft_asalkinerja' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_asalkinerja[]')),
            'tns:ft_asalkinerja'
        );

        //fact table asalkinerja
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_asalkinerja',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_asalkinerja' => array(
                    'name' => 'daftar_ft_asalkinerja', 'type' => 'tns:daftar_ft_asalkinerja'
                )
            )
        );

         //registering create ft asalkinerja
         $this->nusoap_server->register(
            "create_ft_asalkinerja",
            array("data" => 'tns:list_asalkinerja'),
            array("return" => 'tns:fact_table_asalkinerja'),
            "urn:AsalDaerah",
            "urn:AsalDaerah#create_ft_asalkinerja",
            "rpc",
            "encoded",
            "Returning ft asalkinerja"
        );

    }

    function index() {

        function create_dim_angkatan($list_angkatan)
        {
            $data_to_return = array();
            $data_to_return['jenis_dt'] = 0;
            foreach($list_angkatan as $l)
            {
                $tmp_arr = array(
                    'id_dim_angkatan' => $l['angkatan'],
                    'angkatan' => $l['angkatan']
                );
                $data_to_return['daftar_dim_angkatan'][] = $tmp_arr;
            }
            return $data_to_return;
        }

        function create_dim_prodi($list_prodi)
        {
            $data_to_return = array();
            $data_to_return['jenis_dt'] = 0;
            foreach($list_prodi as $l)
            {
                $tmp_arr = array(
                    'id_dim_prodi' => $l['prodi_id'],
                    'nama_prodi' => $l['nama_prodi']
                );
                $data_to_return['daftar_dim_prodi'][] = $tmp_arr;
            }
            return $data_to_return;
        }

        function create_dim_bk($list_bk)
        {
            $data_to_return['jenis_dt'] = 0;
            foreach($list_bk as $l)
            {
                $data_to_return['daftar_dim_bk'][] = array(
                    'id_dim_bidangkompetensi' => $l['bidangkompetensi_id'],
                    'bidangkompetensi'  => $l['bidangkompetensi']
                );
            }
            return $data_to_return;
        }

        function create_dim_jlulusan($list_jlulusan)
        {
            $data_to_return['jenis_dt'] = 0;
            foreach($list_jlulusan as $l)
            {
                $data_to_return['daftar_dim_jlulusan'][] = array(
                    'id_dim_jlulusan'   => $l['id_dim_jlulusan'],
                    'jenis_lulusan'  => $l['status']
                );
            }
            return $data_to_return;
        }

        function create_dim_jenissekolah($list_jk)
        {
            $data_to_return['jenis_dt'] = 0;
            foreach($list_jk as $l)
            {
                $data_to_return['daftar_dim_jenissekolah'][] = array(
                    'id_dim_jenissekolah' => $l['jenissekolah_id'],
                    'jenissekolah'  => $l['jenissekolah']
                );
            }
            return $data_to_return;
        }

        function create_dim_subjalur($list_dim_subjalur)
        {
            $data_to_return['jenis_dt'] = 0;
            foreach($list_dim_subjalur as $l)
            {
                $data_to_return['daftar_dim_subjalur'][] = array(
                    'id_dim_subjalur'   => $l['subjalur_id'],
                    'subjalur'          => $l['subjalur'],
                    'id_dim_jalurmasuk' => $l['jalur_id']
                );
            }
            return $data_to_return;
        }

        function create_dim_asaldaerah($list_dim_asaldaerah)
        {
            $data_to_return['jenis_dt'] = 0;
            foreach($list_dim_asaldaerah as $l)
            {
                $data_to_return['daftar_dim_asaldaerah'][] = array(
                    'id_dim_asaldaerah' => $l['asaldaerah_id'],
                    'kota'              => $l['kota'],
                    'propinsi'          => $l['propinsi']
                );
            }
            return $data_to_return;
        }

        function create_ft_kelulusan($list_kelulusan)
        {
            $CI =& get_instance();
            $daftar_ft_kelulusan = array();
            $daftar_ft_kelulusan['jenis_dt'] =1;
            foreach($list_kelulusan as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_bidangkompetensi'] = $l['bidangkompetensi_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['id_dim_jlulusan'] = $l['status'] == 'TEPAT WAKTU' ? '1' : '2';
                $tmp_arr['jml_lulusan'] = $l['jml_lulusan'];
                $tmp_arr['rata_ipk'] = $l['rata_ipk'];

                $daftar_ft_kelulusan['daftar_ft_kelulusan'][]= $tmp_arr;


            }
            return $daftar_ft_kelulusan;
        }

        function create_ft_ips($list_ips)
        {
            $CI =& get_instance();
            $daftar_ft_ips = array();
            $daftar_ft_ips['jenis_dt'] =1;
            foreach($list_ips as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['id_dim_semester'] = $l['semester'];
                $tmp_arr['jml_kompen'] = $l['jml_kompen'];
                $tmp_arr['rata_ips'] = $l['rata_ips'];

                $daftar_ft_ips['daftar_ft_ips'][]= $tmp_arr;


            }
            return $daftar_ft_ips;
        }


        function create_ft_asalmhs($list_asalmhs)
        {
            $CI =& get_instance();
            $daftar_ft_asalmhs = array();
            $daftar_ft_asalmhs['jenis_dt'] =1;
            foreach($list_asalmhs as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_asaldaerah'] = $l['asaldaerah_id'];
                $tmp_arr['id_dim_subjalur'] = $l['subjalur_id'];
                $tmp_arr['id_dim_jenissekolah'] = $l['jenissekolah_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['jml_pendaftar'] = $l['jml_pendaftar'];
                $tmp_arr['jml_kotaasal'] = $l['jml_kotaasal'];
                $tmp_arr['jml_subjalur'] = $l['jml_subjalur'];
                $tmp_arr['jml_jenissekolah'] = $l['jml_jenissekolah'];

                $daftar_ft_asalmhs['daftar_ft_asalmhs'][]= $tmp_arr;

            }
            return $daftar_ft_asalmhs;
        }

        function create_ft_asalkinerja($list_asalkinerja)
        {
            $CI =& get_instance();
            $daftar_ft_asalkinerja = array();
            $daftar_ft_asalkinerja['jenis_dt'] =1;
            foreach($list_asalkinerja as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_asaldaerah'] = $l['asaldaerah_id'];
                $tmp_arr['id_dim_subjalur'] = $l['subjalur_id'];
                $tmp_arr['id_dim_jenissekolah'] = $l['jenissekolah_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['rata_ipk'] = $l['rata_ipk'];
                $tmp_arr['jml_kompen'] = $l['jml_kompen'];

                $daftar_ft_asalkinerja['daftar_ft_asalkinerja'][]= $tmp_arr;

            }
            return $daftar_ft_asalkinerja;
        }


        // function create_ft_kelulusan($)
         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }

}