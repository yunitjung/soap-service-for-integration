<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class SaveDimension extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('Nusoap_lib');
        $this->load->model('dimensisave_model');
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("SaveDimension", 'urn:SaveDimension2');

         //daftar of dim prodi
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_prodi',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_prodi' => array('name' => 'dim_prodi', 'type' => 'tns:dim_prodi')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_prodi[]')),
            'tns:dim_prodi'
        );

        //list of dim prodi with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_prodi',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_prodi' => array(
                    'name' => 'daftar_dim_prodi', 'type' => 'tns:daftar_dim_prodi'
                )
            )
        );

        $this->nusoap_server->register(
            "save_prodi_to_db",
            array('list_dim_prodi' => 'tns:list_dim_prodi'),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_prodi_to_db",
            "rpc",
            'encoded',
            "Inserting data dim prodi to data warehouse"
        );


    }

    function index()
    {
        function save_prodi_to_db($list_dim_prodi)
        {
            $CI =& get_instance();
            $flag = 1;
            foreach($list_dim_prodi as $l){
                $check_id = $CI->dimensisave_model->check_id('dim_prodi', array('id_dim_prodi' => $l['id_dim_prodi']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_prodi($l);
                    $flag = $result ? 1 : 0;
                }
                else $flag = 1;
            }
            return $flag;
        }

        function save_bk_to_db($list_dim_bk)
        {
            $CI =& get_instance();
            $flag = 1;
            foreach($list_dim_bk as $l){
                $check_id = $CI->dimensisave_model->check_id('dim_bidangkompetensi', array('id_dim_bidangkompetensi' => $l['id_dim_bidangkompetensi']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_bk($l);
                    $flag = $result ? 1 : 0;
                }
                else $flag = 1;
            }
            return $flag;
        }

        function save_jlulusan_to_db($list_dim_jlulusan)
        {
            $CI =& get_instance();
            $flag = 1;
            foreach($list_dim_jlulusan as $l)
            {
                $check_id = $CI->dimensisave_model->check_id('dim_jlulusan', array('id_dim_jlulusan' => $l['id_dim_jlulusan']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_jlulusan($l);
                    $flag = $result ? 1 : 0;
                }
                else $flag = 1;
            }
            return $flag;
        }

        function save_asaldaerah_to_db($list_dim_asaldaerah)
        {
            $CI =& get_instance();
            $flag = 1;
            foreach($list_dim_asaldaerah as $l)
            {
                $check_id = $CI->dimensisave_model->check_id('dim_asaldaerah', array('id_dim_asaldaerah' => $l['id_dim_asaldaerah']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_asaldaerah($l);
                    $flag = $result ? 1 : 0;
                }
                else $flag = 1;
            }
            return $flag;
        }

        function save_subjalur_to_db($list_dim_subjalur)
        {
            $CI =& get_instance();
            $flag = 1;
            foreach($list_dim_subjalur as $l)
            {

                $check_id = $CI->dimensisave_model->check_id('dim_subjalur', array('id_dim_subjalur' => $l['id_dim_subjalur']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_subjalur_to_db($l);
                    $flag = $result ? 1 : 0;
                }
                else $flag = 1;
            }
            return $flag;
        }

        function save_dim_jenissekolah($list_jk)
        {
            $CI =& get_instance();
            $flag = 1;
            foreach($list_jk as $j)
            {
                $check_id = $CI->dimensisave_model->check_id('dim_jenissekolah', array('id_dim_jenissekolah' => $j['id_dim_jenissekolah']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_jenissekolah_to_db($j);
                    $flag = $result ? 1 : 0;

                }
                else $flag =  $result;
            }
            return $flag;
        }

        $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);

    }
}
