<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class SubJalur extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Subjalur", "urn:Subjalur");

         //req subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'req_subjalur',
            'complexType',
            'array',
            'all',
            '',
            array(
                'subjalur_id' => array('name' => 'subjalur_id', 'type' => 'xsd:int'),
                'subjalur' => array('name' => 'subjalur', 'type' => 'xsd:string'),
                'jalur_id' => array('name' => 'jalur_id', 'type' => 'xsd:string')
            )
        );

         //list of subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'list_subjalur',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_subjalur' => array('name' => 'req_subjalur', 'type' => 'tns:req_subjalur')),
            array(
                'req_subjalur' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_subjalur[]')),
            'tns:req_subjalur'
        );

         //array for dim_subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'dim_subjalur',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_subjalur' => array(
                    'name' => 'id_dim_subjalur', 'type' => 'xsd:int'
                ),
                'subjalur' => array(
                    'name' => 'subjalur', 'type' => 'xsd:string'
                ),
                'id_dim_jalurmasuk' => array(
                    'name' => 'id_dim_jalurmasuk', 'type' => 'xsd:string'
                )
            )
        );

         //daftar of dim_subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_subjalur',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_subjalur' => array('name' => 'dim_subjalur', 'type' => 'tns:dim_subjalur')),
            array(
                'dim_subjalur' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_subjalur[]')),
            'tns:dim_subjalur'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_subjalur',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_subjalur' => array(
                    'name' => 'daftar_dim_subjalur', 'type' => 'tns:daftar_dim_subjalur'
                )
            )
        );


         //registering create_dim_subjalur
         $this->nusoap_server->register(
            "create_dim_subjalur",
            array("data" => 'tns:list_subjalur'),
            array("return" => 'tns:list_dim_subjalur'),
            "urn:Service1",
            "urn:Service1#create_dim_subjalur",
            "rpc",
            "encoded",
            "Returning dim subjalur"
        );



    }

    function index() {

        function create_dim_subjalur($list_dim_subjalur)
        {
            foreach($list_dim_subjalur as $l)
            {
                $data_to_return['daftar_dim_subjalur'][] = array(
                    'id_dim_subjalur'   => $l['subjalur_id'],
                    'subjalur'          => $l['subjalur'],
                    'id_dim_jalurmasuk' => $l['jalur_id']
                );
            }
            return $data_to_return;
        }


         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}