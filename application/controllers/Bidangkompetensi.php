<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bidangkompetensi extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Bidangkompetensi", "urn:Bidangkompetensi");

        /*bidangkompetensi*/
        //req bidang kompetensi
        $this->nusoap_server->wsdl->addComplexType(
            'req_bk',
            'complexType',
            'array',
            'all',
            '',
            array(
                'id_dim_bidangkompetensi'=> array('name' => 'id_dim_bidangkompetensi', 'type' => 'xsd:int'),
                'bidangkompetensi'       => array('name' => 'bidangkompetensi', 'type' => 'xsd:string'),
                'status'                 => array('name' => 'status', 'type' => 'xsd:string')
            )
        );

        //list of bk
        $this->nusoap_server->wsdl->addComplexType(
        'list_bk',
        'complexType', 'array', '', 'SOAP-ENC:Array',
        array(
            'req_bk' => array('name' => 'req_bk', 'type' => 'tns:req_bk')),
        array(
            'req_bk' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_bk[]')),
        'tns:req_bk'
        );

        //array for dim_bk
        $this->nusoap_server->wsdl->addComplexType(
            'dim_bk',
            'complexType',
            'struct',
            'all',
            '',
            array(
                    'id_dim_bidangkompetensi' => array(
                        'name' => 'id_dim_bidangkompetensi', 'type' => 'xsd:int'
                    ),
                    'bidangkompetensi' => array(
                        'name' => 'bidangkompetensi', 'type' => 'xsd:string')
            )
        );

        //daftar of dim bk
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_bk',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_bk' => array('name' => 'dim_bk', 'type' => 'tns:dim_bk')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_bk[]')),
            'tns:dim_bk'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_bk',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_bk' => array(
                    'name' => 'daftar_dim_bk', 'type' => 'tns:daftar_dim_bk'
                )
            )
        );

        //registering create_dim_bk
        $this->nusoap_server->register(
            "create_dim_bk",
            array("data" => 'tns:list_bk'),
            array("return" => 'tns:list_dim_bk'),
            "urn:Service1",
            "urn:Service1#create_dim_bk",
            "rpc",
            "encoded",
            "Returning dim bk"
        );



    }

    function index() {

        function create_dim_bk($list_bk)
        {
            foreach($list_bk as $l)
            {
                $data_to_return['daftar_dim_bk'][] = array(
                    'id_dim_bidangkompetensi' => $l['bidangkompetensi_id'],
                    'bidangkompetensi'  => $l['bidangkompetensi']
                );
            }
            return $data_to_return;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}