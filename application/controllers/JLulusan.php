<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class JLulusan extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("JLulusan", "urn:JLulusan");

        //req jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'req_jlulusan',
            'complexType',
            'array',
            'all',
            '',
            array(
                'id_dim_jlulusan'        => array('name' => 'id_dim_jlulusan', 'type' => 'xsd:int'),
                'status'                 => array('name' => 'status', 'type' => 'xsd:string')
            )
        );

        //list of jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'list_jlulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_jlulusan' => array('name' => 'req_jlulusan', 'type' => 'tns:req_jlulusan')),
            array(
                'req_jlulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_jlulusan[]')),
            'tns:req_jlulusan'
            );

        //array for dim_jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'dim_jlulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                    'id_dim_jlulusan' => array(
                        'name' => 'id_dim_jlulusan', 'type' => 'xsd:int'
                    ),
                    'jenis_lulusan' => array(
                        'name' => 'jenis_lulusan', 'type' => 'xsd:string')
            )
        );

        //daftar of dim_jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_jlulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_jlulusan' => array('name' => 'dim_jlulusan', 'type' => 'tns:dim_jlulusan')),
            array(
                'dim_jlulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_jlulusan[]')),
            'tns:dim_jlulusan'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_jlulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_jlulusan' => array(
                    'name' => 'daftar_dim_jlulusan', 'type' => 'tns:daftar_dim_jlulusan'
                )
            )
        );

        //registering create_dim_jlulusan
        $this->nusoap_server->register(
            "create_dim_jlulusan",
            array("data" => 'tns:list_jlulusan'),
            array("return" => 'tns:list_dim_jlulusan'),
            "urn:Service1",
            "urn:Service1#create_dim_jlulusan",
            "rpc",
            "encoded",
            "Returning dim bk"
        );

        //req kelulusan array
        $this->nusoap_server->wsdl->addComplexType(
            'req_kelulusan',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'bidangkompetensi_id' => array('name' => 'bidangkompetensi_id', 'type' => 'xsd:string'),
                'status'      => array('name' => 'status', 'type' => 'xsd:string'),
                'jml_lulusan'      => array('name' => 'jml_lulusan', 'type' => 'xsd:string'),
                'rata_ipk'      => array('name' => 'rata_ipk', 'type' => 'xsd:string')
            )
        );

        //list of req kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'list_kelulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_kelulusan' => array('name' => 'req_kelulusan', 'type' => 'tns:req_kelulusan')),
            array(
                'req_kelulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_kelulusan[]')),
            'tns:req_kelulusan'
            );

        //return ft kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'ft_kelulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_bidangkompetensi' => array(
                    'name' => 'id_dim_bidangkompetensi', 'type' => 'xsd:string'
                ),
                'id_dim_jlulusan' => array(
                    'name' => 'id_dim_jlulusan', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'rata_ipk' => array(
                    'name' => 'rata_ipk', 'type' => 'xsd:int'
                ),
                'jml_lulusan' => array(
                    'name' => 'jml_lulusan', 'type' => 'xsd:double'
                )

            )
        );

        //list of ft kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_kelulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_kelulusan' => array('name' => 'ft_kelulusan', 'type' => 'tns:ft_kelulusan')),
            array(
                'ft_kelulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_kelulusan[]')),
            'tns:ft_kelulusan'
        );

        //fact table kelulusan
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_kelulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_kelulusan' => array(
                    'name' => 'daftar_ft_kelulusan', 'type' => 'tns:daftar_ft_kelulusan'
                )
            )
        );

         //registering create ft kelulusan
         $this->nusoap_server->register(
            "create_ft_kelulusan",
            array("data" => 'tns:list_kelulusan'),
            array("return" => 'tns:fact_table_kelulusan'),
            "urn:Service1",
            "urn:Service1#create_ft_kelulusan",
            "rpc",
            "encoded",
            "Returning ft kelulusan"
        );



    }

    function index() {

        function create_dim_jlulusan($list_jlulusan)
        {
            foreach($list_jlulusan as $l)
            {
                $data_to_return['daftar_dim_jlulusan'][] = array(
                    'id_dim_jlulusan'   => $l['id_dim_jlulusan'],
                    'jenis_lulusan'  => $l['status']
                );
            }
            return $data_to_return;
        }

        function create_ft_kelulusan($list_kelulusan)
        {
            $CI =& get_instance();
            $daftar_ft_kelulusan = array();
            $daftar_ft_kelulusan['jenis_dt'] =1;
            foreach($list_kelulusan as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_bidangkompetensi'] = $l['bidangkompetensi_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['id_dim_jlulusan'] = $l['status'] == 'TEPAT WAKTU' ? '1' : '2';
                $tmp_arr['jml_lulusan'] = $l['jml_lulusan'];
                $tmp_arr['rata_ipk'] = $l['rata_ipk'];

                $daftar_ft_kelulusan['daftar_ft_kelulusan'][]= $tmp_arr;


            }
            return $daftar_ft_kelulusan;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}