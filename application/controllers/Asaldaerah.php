<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Asaldaerah extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Asaldaerah", "urn:Asaldaerah");

       //req asaldaerah
       $this->nusoap_server->wsdl->addComplexType(
            'req_asaldaerah',
            'complexType',
            'array',
            'all',
            '',
            array(
                'asaldaerah_id' => array('name' => 'asaldaerah_id', 'type' => 'xsd:string'),
                'kota_siswa' => array('name' => 'kota_siswa', 'type' => 'xsd:string'),
                'propinsi_siswa' => array('name' => 'propinsi_siswa', 'type' => 'xsd:string')
            )
        );

        //list of asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'list_asaldaerah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_asaldaerah' => array('name' => 'req_asaldaerah', 'type' => 'tns:req_asaldaerah')),
            array(
                'req_asaldaerah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_asaldaerah[]')),
            'tns:req_asaldaerah'
            );

        //array for dim_asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'dim_asaldaerah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_asaldaerah' => array('name' => 'id_dim_asaldaerah', 'type' => 'xsd:string'),
                'kota' => array(
                    'name' => 'kota', 'type' => 'xsd:string'
                ),
                'propinsi' => array(
                    'name' => 'propinsi', 'type' => 'xsd:string'
                )
            )
        );

        //daftar of dim_asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_asaldaerah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_asaldaerah' => array('name' => 'dim_asaldaerah', 'type' => 'tns:dim_asaldaerah')),
            array(
                'dim_asaldaerah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_asaldaerah[]')),
            'tns:dim_asaldaerah'
        );

        //list of dim_asaldaerah with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_asaldaerah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_asaldaerah' => array(
                    'name' => 'daftar_dim_asaldaerah', 'type' => 'tns:daftar_dim_asaldaerah'
                )
            )
        );

        //registering create_dim_asaldaerah
        $this->nusoap_server->register(
            "create_dim_asaldaerah",
            array("data" => 'tns:list_asaldaerah'),
            array("return" => 'tns:list_dim_asaldaerah'),
            "urn:Service1",
            "urn:Service1#create_dim_asaldaerah",
            "rpc",
            "encoded",
            "Returning dim asaldaerah"
        );

         //req asalmhs array
         $this->nusoap_server->wsdl->addComplexType(
            'req_asalmhs',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'asaldaerah_id'           => array('name' => 'asaldaerah_id', 'type' => 'xsd:string'),
                'subjalur_id'           => array('name' => 'subjalur_id', 'type' => 'xsd:string'),
                'jenissekolah_id'           => array('name' => 'jenissekolah_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'jml_pendaftar'      => array('name' => 'jml_pendaftar', 'type' => 'xsd:string'),
                'jml_kotaasal'      => array('name' => 'jml_kotaasal', 'type' => 'xsd:string'),
                'jml_subjalur'      => array('name' => 'jml_subjalur', 'type' => 'xsd:string'),
                'jml_jenissekolah'      => array('name' => 'jml_jenissekolah', 'type' => 'xsd:string')

            )
        );

        //list of req asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'list_asalmhs',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_asalmhs' => array('name' => 'req_asalmhs', 'type' => 'tns:req_asalmhs')),
            array(
                'req_asalmhs' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_asalmhs[]')),
            'tns:req_asalmhs'
            );

        //return ft asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'ft_asalmhs',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_asaldaerah' => array(
                    'name' => 'id_dim_asaldaerah', 'type' => 'xsd:string'
                ),
                'id_dim_subjalur' => array(
                    'name' => 'id_dim_subjalur', 'type' => 'xsd:string'
                ),
                'id_dim_jenissekolah' => array(
                    'name' => 'id_dim_jenissekolah', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'jml_pendaftar' => array(
                    'name' => 'jml_pendaftar', 'type' => 'xsd:int'
                ),
                'jml_kotaasal' => array(
                    'name' => 'jml_kotaasal', 'type' => 'xsd:int'
                ),
                'jml_subjalur' => array(
                    'name' => 'jml_subjalur', 'type' => 'xsd:int'
                ),
                'jml_jenissekolah' => array(
                    'name' => 'jml_jenissekolah', 'type' => 'xsd:int'
                )
            )
        );

        //list of ft asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_asalmhs',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_asalmhs' => array('name' => 'ft_asalmhs', 'type' => 'tns:ft_asalmhs')),
            array(
                'ft_asalmhs' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_asalmhs[]')),
            'tns:ft_asalmhs'
        );

        //fact table asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_asalmhs',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_asalmhs' => array(
                    'name' => 'daftar_ft_asalmhs', 'type' => 'tns:daftar_ft_asalmhs'
                )
            )
        );

         //registering create ft asalmhs
         $this->nusoap_server->register(
            "create_ft_asalmhs",
            array("data" => 'tns:list_asalmhs'),
            array("return" => 'tns:fact_table_asalmhs'),
            "urn:AsalDaerah",
            "urn:AsalDaerah#create_ft_asalmhs",
            "rpc",
            "encoded",
            "Returning ft asalmhs"
        );


         //req asalkinerja array
         $this->nusoap_server->wsdl->addComplexType(
            'req_asalkinerja',
            'element',
            'struct',
            'all',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string'),
                'prodi_id'           => array('name' => 'prodi_id', 'type' => 'xsd:string'),
                'asaldaerah_id'           => array('name' => 'asaldaerah_id', 'type' => 'xsd:string'),
                'subjalur_id'           => array('name' => 'subjalur_id', 'type' => 'xsd:string'),
                'jenissekolah_id'           => array('name' => 'jenissekolah_id', 'type' => 'xsd:string'),
                'jenis_kelamin'       => array('name' => 'jenis_kelamin', 'type' => 'xsd:string'),
                'rata_ipk'      => array('name' => 'rata_ipk', 'type' => 'xsd:string'),
                'jml_kompen'      => array('name' => 'jml_kompen', 'type' => 'xsd:string')
            )
        );

        //list of req asalmhs
        $this->nusoap_server->wsdl->addComplexType(
            'list_asalkinerja',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_asalkinerja' => array('name' => 'req_asalkinerja', 'type' => 'tns:req_asalkinerja')),
            array(
                'req_asalkinerja' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_asalkinerja[]')),
            'tns:req_asalkinerja'
            );

        //return ft asalkinerja
        $this->nusoap_server->wsdl->addComplexType(
            'ft_asalkinerja',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan' => array(
                    'name' => 'id_dim_angkatan', 'type' => 'xsd:string'
                ),
                'id_dim_prodi' => array(
                    'name' => 'id_dim_prodi', 'type' => 'xsd:int'
                ),
                'id_dim_asaldaerah' => array(
                    'name' => 'id_dim_asaldaerah', 'type' => 'xsd:string'
                ),
                'id_dim_subjalur' => array(
                    'name' => 'id_dim_subjalur', 'type' => 'xsd:string'
                ),
                'id_dim_jenissekolah' => array(
                    'name' => 'id_dim_jenissekolah', 'type' => 'xsd:string'
                ),
                'id_dim_jeniskelamin' => array(
                    'name' => 'id_dim_jeniskelamin', 'type' => 'xsd:string'
                ),
                'rata_ipk' => array(
                    'name' => 'rata_ipk', 'type' => 'xsd:double'
                ),
                'jml_kompen' => array(
                    'name' => 'jml_kompen', 'type' => 'xsd:int'
                )
            )
        );

        //list of ft asalkinerja
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_asalkinerja',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_asalkinerja' => array('name' => 'ft_asalkinerja', 'type' => 'tns:ft_asalkinerja')),
            array(
                'ft_asalkinerja' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_asalkinerja[]')),
            'tns:ft_asalkinerja'
        );

        //fact table asalkinerja
        $this->nusoap_server->wsdl->addComplexType(
            'fact_table_asalkinerja',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_asalkinerja' => array(
                    'name' => 'daftar_ft_asalkinerja', 'type' => 'tns:daftar_ft_asalkinerja'
                )
            )
        );

         //registering create ft asalkinerja
         $this->nusoap_server->register(
            "create_ft_asalkinerja",
            array("data" => 'tns:list_asalkinerja'),
            array("return" => 'tns:fact_table_asalkinerja'),
            "urn:AsalDaerah",
            "urn:AsalDaerah#create_ft_asalkinerja",
            "rpc",
            "encoded",
            "Returning ft asalkinerja"
        );

    }

    function index() {

        function create_dim_asaldaerah($list_dim_asaldaerah)
        {
            foreach($list_dim_asaldaerah as $l)
            {
                $data_to_return['daftar_dim_asaldaerah'][] = array(
                    'id_dim_asaldaerah' => $l['asaldaerah_id'],
                    'kota'              => $l['kota'],
                    'propinsi'          => $l['propinsi']
                );
            }
            return $data_to_return;
        }

        function create_ft_asalmhs($list_asalmhs)
        {
            $CI =& get_instance();
            $daftar_ft_asalmhs = array();
            $daftar_ft_asalmhs['jenis_dt'] =1;
            foreach($list_asalmhs as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_asaldaerah'] = $l['asaldaerah_id'];
                $tmp_arr['id_dim_subjalur'] = $l['subjalur_id'];
                $tmp_arr['id_dim_jenissekolah'] = $l['jenissekolah_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['jml_pendaftar'] = $l['jml_pendaftar'];
                $tmp_arr['jml_kotaasal'] = $l['jml_kotaasal'];
                $tmp_arr['jml_subjalur'] = $l['jml_subjalur'];
                $tmp_arr['jml_jenissekolah'] = $l['jml_jenissekolah'];

                $daftar_ft_asalmhs['daftar_ft_asalmhs'][]= $tmp_arr;

            }
            return $daftar_ft_asalmhs;
        }

        function create_ft_asalkinerja($list_asalkinerja)
        {
            $CI =& get_instance();
            $daftar_ft_asalkinerja = array();
            $daftar_ft_asalkinerja['jenis_dt'] =1;
            foreach($list_asalkinerja as $l)
            {
                $tmp_arr = array();
                $tmp_arr['id_dim_angkatan'] = $l['angkatan'];
                $tmp_arr['id_dim_prodi'] = $l['prodi_id'];
                $tmp_arr['id_dim_asaldaerah'] = $l['asaldaerah_id'];
                $tmp_arr['id_dim_subjalur'] = $l['subjalur_id'];
                $tmp_arr['id_dim_jenissekolah'] = $l['jenissekolah_id'];
                $tmp_arr['id_dim_jeniskelamin'] =  $l['jenis_kelamin'] == 'L' ? '2' : '1';
                $tmp_arr['rata_ipk'] = $l['rata_ipk'];
                $tmp_arr['jml_kompen'] = $l['jml_kompen'];

                $daftar_ft_asalkinerja['daftar_ft_asalkinerja'][]= $tmp_arr;

            }
            return $daftar_ft_asalkinerja;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}