<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class FactTableSave extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('Nusoap_lib');
        $this->load->model('facttablesave_model', 'm');
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("FactTableSave", 'urn:FactTableSave');

       //daftar of ft_ips
       $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_ips',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_ips' => array('name' => 'ft_ips', 'type' => 'tns:ft_ips')),
            array(
                'ft_ips' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_ips[]')),
            'tns:ft_ips'
        );

        //list of ft_ips with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_ft_ips',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_ips' => array(
                    'name' => 'daftar_ft_ips', 'type' => 'tns:daftar_ft_ips'
                )
            )
        );

        $this->nusoap_server->register(
            "save_ft_ips",
            array("list_ft_ips" => 'tns:list_ft_ips'),
            array("return" => "xsd:int"),
            "urn:FactTableSave",
            "urn:FactTableSave#save_ft_ips",
            "rpc",
            'encoded',
            "Inserting data ft ips to data warehouse"
        );

        //daftar of ft_kelulusan
       $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_kelulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_kelulusan' => array('name' => 'ft_kelulusan', 'type' => 'tns:ft_kelulusan')),
            array(
                'ft_kelulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_kelulusan[]')),
            'tns:ft_kelulusan'
        );

        //list of ft_kelulusan with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_ft_kelulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_kelulusan' => array(
                    'name' => 'daftar_ft_kelulusan', 'type' => 'tns:daftar_ft_kelulusan'
                )
            )
        );



        $this->nusoap_server->register(
            "save_ft_kelulusan",
            array("list_ft_kelulusan" => 'tns:list_ft_kelulusan'),
            array("return" => "xsd:int"),
            "urn:FactTableSave",
            "urn:FactTableSave#list_ft_kelulusan",
            "rpc",
            'encoded',
            "Inserting data ft_kelulusan to data warehouse"
        );

          //daftar of ft_asalmhs
       $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_asalmhs',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_asalmhs' => array('name' => 'ft_asalmhs', 'type' => 'tns:ft_asalmhs')),
            array(
                'ft_asalmhs' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_asalmhs[]')),
            'tns:ft_asalmhs'
        );

        //list of ft_asalmhs with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_ft_asalmhs',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_ft_asalmhs' => array(
                    'name' => 'daftar_ft_asalmhs', 'type' => 'tns:daftar_ft_asalmhs'
                )
            )
        );

        $this->nusoap_server->register(
            "save_ft_asalmhs",
            array("list_ft_asalmhs" => 'tns:list_ft_asalmhs'),
            array("return" => "xsd:int"),
            "urn:FactTableSave",
            "urn:FactTableSave#list_ft_asalmhs",
            "rpc",
            'encoded',
            "Inserting data ft_asalmhs to data warehouse"
        );

         //list of ft asalkinerja
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_ft_asalkinerja',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'ft_asalkinerja' => array('name' => 'ft_asalkinerja', 'type' => 'tns:ft_asalkinerja')),
            array(
                'ft_asalkinerja' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:ft_asalkinerja[]')),
            'tns:ft_asalkinerja'
        );


        $this->nusoap_server->register(
            "save_ft_asalkinerja",
            array("daftar_ft_asalkinerja" => 'tns:daftar_ft_asalkinerja'),
            array("return" => "xsd:int"),
            "urn:FactTableSave",
            "urn:FactTableSave#daftar_ft_asalkinerja",
            "rpc",
            'encoded',
            "Inserting data ft_asalkinerja to data warehouse"
        );
    }

    function index()
    {

        function save_ft_ips($daftar_ft_ips)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($daftar_ft_ips as $d)
            {
                $result = $CI->m->check_facttable('fact_ips', $d);
                if(!$result)
                {
                    $results =  $CI->m->insert_ft_ips($d);
                    $flag = $results ? 200 : 500;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_ft_kelulusan($daftar_ft_kelulusan)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($daftar_ft_kelulusan as $d)
            {
                $result = $CI->m->check_facttable('fact_kelulusan', $d);
                if(!$result)
                {
                    $results = $CI->m->insert_ft_kelulusan($d);
                    $flag = $results ? 200 : 500;

                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_ft_asalmhs($daftar_ft_asalmhs)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($daftar_ft_asalmhs as $d)
            {
                $result = $CI->m->check_facttable('fact_asalmhs', $d);
                if(!$result)
                {
                    $results = $CI->m->insert_ft_asalmhs($d);
                    $flag = $results ? 200 : 500;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_ft_asalkinerja($daftar_ft_asalkinerja)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($daftar_ft_asalkinerja as $d)
            {
                $result = $CI->m->check_facttable('fact_asalkinerja', $d);
                if(!$result)
                {
                    $results = $CI->m->insert_ft_asalkinerja($d);
                    $flag = $results ? 200 : 500;
                }
                else $flag = 200;
            }
            return $flag;
        }

        $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
    }


}