<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class DimensiSave extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('Nusoap_lib');
        $this->load->model('dimensisave_model');
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("DimensiSave", 'urn:SaveDimension');


         //daftar of dim angkatan
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_angkatan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_angkatan' => array('name' => 'dim_angkatan', 'type' => 'tns:dim_angkatan')),
            array(
                'dim_angkatan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_angkatan[]')),
            'tns:dim_angkatan'
        );

        //list of dim angkatan with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_angkatan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_angkatan' => array(
                    'name' => 'daftar_dim_angkatan', 'type' => 'tns:daftar_dim_angkatan'
                )
            )
        );

        $this->nusoap_server->register(
            "save_angkatan_to_db",
            array('list_dim_angkatan' => 'tns:list_dim_angkatan'),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_angkatan_to_db",
            "rpc",
            'encoded',
            "Inserting data dim angkatan to data warehouse"
        );

         //daftar of dim prodi
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_prodi',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_prodi' => array('name' => 'dim_prodi', 'type' => 'tns:dim_prodi')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_prodi[]')),
            'tns:dim_prodi'
        );

        //list of dim prodi with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_prodi',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_prodi' => array(
                    'name' => 'daftar_dim_prodi', 'type' => 'tns:daftar_dim_prodi'
                )
            )
        );

        $this->nusoap_server->register(
            "save_prodi_to_db",
            array('list_dim_prodi' => 'tns:list_dim_prodi'),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_prodi_to_db",
            "rpc",
            'encoded',
            "Inserting data dim prodi to data warehouse"
        );


         //daftar of dim bk
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_bk',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_bk' => array('name' => 'dim_bk', 'type' => 'tns:dim_bk')),
            array(
                'dim_prodi' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_bk[]')),
            'tns:dim_bk'
        );

        //list of dim bk with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_bk',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_bk' => array(
                    'name' => 'daftar_dim_bk', 'type' => 'tns:daftar_dim_bk'
                )
            )
        );

        $this->nusoap_server->register(
            "save_bk_to_db",
            array('list_dim_bk' => 'tns:list_dim_bk'),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_bk_to_db",
            "rpc",
            'encoded',
            "Inserting data dim bk to data warehouse"
        );

        //daftar of dim_jlulusan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_jlulusan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_jlulusan' => array('name' => 'dim_jlulusan', 'type' => 'tns:dim_jlulusan')),
            array(
                'dim_jlulusan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_jlulusan[]')),
            'tns:dim_jlulusan'
        );

        //list of dim jlulusan with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_jlulusan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_jlulusan' => array(
                    'name' => 'daftar_dim_jlulusan', 'type' => 'tns:daftar_dim_jlulusan'
                )
            )
        );


        $this->nusoap_server->register(
            "save_jlulusan_to_db",
            array('list_dim_jlulusan' => 'tns:list_dim_jlulusan'),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#jenis_lulusan",
            "rpc",
            'encoded',
            "Inserting data dim jlulusan to data warehouse"
        );

        //daftar of dim_asaldaerah
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_asaldaerah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_asaldaerah' => array('name' => 'dim_asaldaerah', 'type' => 'tns:dim_asaldaerah')),
            array(
                'dim_asaldaerah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_asaldaerah[]')),
            'tns:dim_asaldaerah'
        );

        //list of dim_asaldaerah with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_asaldaerah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_asaldaerah' => array(
                    'name' => 'daftar_dim_asaldaerah', 'type' => 'tns:daftar_dim_asaldaerah'
                )
            )
        );

        $this->nusoap_server->register(
            "save_asaldaerah_to_db",
            array(
                "list_dim_asaldaerah" => "tns:list_dim_asaldaerah"
            ),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_asaldaerah_to_db",
            "rpc",
            'encoded',
            "Inserting data dim asal daerah to data warehouse"
        );

         //daftar of dim_subjalur
         $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_subjalur',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_subjalur' => array('name' => 'dim_subjalur', 'type' => 'tns:dim_subjalur')),
            array(
                'dim_subjalur' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_subjalur[]')),
            'tns:dim_subjalur'
        );

        //list of dim_subjalur with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_subjalur',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_subjalur' => array(
                    'name' => 'daftar_dim_subjalur', 'type' => 'tns:daftar_dim_subjalur'
                )
            )
        );

        $this->nusoap_server->register(
            "save_subjalur_to_db",
            array('list_dim_subjalur' => 'tns:list_dim_subjalur'),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_subjalur_to_db",
            "rpc",
            'encoded',
            "Inserting data dim subjalur to data warehouse"
        );


        //daftar of dim_jenissekolah
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_jenissekolah',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_jenissekolah' => array('name' => 'dim_jenissekolah', 'type' => 'tns:dim_jenissekolah')),
            array(
                'dim_jenissekolah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_jenissekolah[]')),
            'tns:dim_jenissekolah'
        );

        //list of dim_jenissekolah with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_jenissekolah',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'daftar_dim_jenissekolah' => array(
                    'name' => 'daftar_dim_jenissekolah', 'type' => 'tns:daftar_dim_jenissekolah'
                )
            )
        );


        $this->nusoap_server->register(
            "save_dim_jenissekolah",
            array(
                'daftar_dim_jenissekolah' => 'tns:daftar_dim_jenissekolah'
            ),
            array("return" => 'xsd:int'),
            "urn:SaveDimension",
            "urn:SaveDimension#save_dim_jenissekolah",
            "rpc",
            'encoded',
            "Inserting data dim jenis sekolah to data warehouse"
        );
    }

    function index()
    {
        function save_angkatan_to_db($list_dim_angkatan)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_dim_angkatan as $l){
                $check_id = $CI->dimensisave_model->check_id('dim_angkatan', array('id_dim_angkatan' => $l['id_dim_angkatan']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_angkatan($l);
                    $flag = $result ? 500 : 200;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_prodi_to_db($list_dim_prodi)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_dim_prodi as $l){
                $check_id = $CI->dimensisave_model->check_id('dim_prodi', array('id_dim_prodi' => $l['id_dim_prodi']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_prodi($l);
                    $flag = $result ? 500 : 200;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_bk_to_db($list_dim_bk)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_dim_bk as $l){
                $check_id = $CI->dimensisave_model->check_id('dim_bidangkompetensi', array('id_dim_bidangkompetensi' => $l['id_dim_bidangkompetensi']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_bk($l);
                    $flag = $result ? 500 : 200;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_jlulusan_to_db($list_dim_jlulusan)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_dim_jlulusan as $l)
            {
                $check_id = $CI->dimensisave_model->check_id('dim_jlulusan', array('id_dim_jlulusan' => $l['id_dim_jlulusan']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_jlulusan($l);
                    $flag = $result ? 500 : 200;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_asaldaerah_to_db($list_dim_asaldaerah)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_dim_asaldaerah as $l)
            {
                $check_id = $CI->dimensisave_model->check_id('dim_asaldaerah', array('id_dim_asaldaerah' => $l['id_dim_asaldaerah']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_dim_asaldaerah($l);
                    $flag = $result ? 500 : 200;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_subjalur_to_db($list_dim_subjalur)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_dim_subjalur as $l)
            {

                $check_id = $CI->dimensisave_model->check_id('dim_subjalur', array('id_dim_subjalur' => $l['id_dim_subjalur']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_subjalur_to_db($l);
                    $flag = $result ? 500 : 200;
                }
                else $flag = 200;
            }
            return $flag;
        }

        function save_dim_jenissekolah($list_jk)
        {
            $CI =& get_instance();
            $flag = 200;
            foreach($list_jk as $j)
            {
                $check_id = $CI->dimensisave_model->check_id('dim_jenissekolah', array('id_dim_jenissekolah' => $j['id_dim_jenissekolah']));
                if($check_id)
                {
                    $result = $CI->dimensisave_model->save_jenissekolah_to_db($j);
                    $flag = $result ? 500 : 200;

                }
                else $flag =  200;
            }
            return $flag;
        }

        $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);

    }
}
