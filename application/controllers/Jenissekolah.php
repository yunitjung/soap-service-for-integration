<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class JenisSekolah extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Jenissekolah", "urn:Jenissekolah");

      //req jenissekolah
      $this->nusoap_server->wsdl->addComplexType(
        'req_jenissekolah',
        'complexType',
        'array',
        'all',
        '',
        array(
            'id_dim_jenissekolah' => array(
                'name' => 'id_dim_jenissekolah', 'type' => 'xsd:int'
            ),
            'jenissekolah' => array('name' => 'jenissekolah', 'type' => 'xsd:string'),
        )
    );

    //list of jenissekolah
    $this->nusoap_server->wsdl->addComplexType(
        'list_jenissekolah',
        'complexType', 'array', '', 'SOAP-ENC:Array',
        array(
            'req_jenissekolah' => array('name' => 'req_jenissekolah', 'type' => 'tns:req_jenissekolah')),
        array(
            'req_jenissekolah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_jenissekolah[]')),
        'tns:req_jenissekolah'
        );

     //array for dim_jenissekolah
     $this->nusoap_server->wsdl->addComplexType(
        'dim_jenissekolah',
        'complexType',
        'struct',
        'all',
        '',
        array(
                'id_dim_jenissekolah' => array(
                    'name' => 'id_dim_jenissekolah', 'type' => 'xsd:int'
                ),
                'jenissekolah' => array(
                    'name' => 'jenissekolah', 'type' => 'xsd:string')
        )
    );

     //daftar of dim_jenissekolah
     $this->nusoap_server->wsdl->addComplexType(
        'daftar_dim_jenissekolah',
        'complexType', 'array', '', 'SOAP-ENC:Array',
        array(
            'dim_jenissekolah' => array('name' => 'dim_jenissekolah', 'type' => 'tns:dim_jenissekolah')),
        array(
            'dim_jenissekolah' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_jenissekolah[]')),
        'tns:dim_jenissekolah'
    );

    //list of dim_jenissekolah with identifier
    $this->nusoap_server->wsdl->addComplexType(
        'list_dim_jenissekolah',
        'complexType',
        'struct',
        'all',
        '',
        array(
            'daftar_dim_jenissekolah' => array(
                'name' => 'daftar_dim_jenissekolah', 'type' => 'tns:daftar_dim_jenissekolah'
            )
        )
    );


    //registering create_dim_jenissekolah
    $this->nusoap_server->register(
        "create_dim_jenissekolah",
        array("data" => 'tns:list_jenissekolah'),
        array("return" => 'tns:list_dim_jenissekolah'),
        "urn:Service1",
        "urn:Service1#create_dim_jenissekolah",
        "rpc",
        "encoded",
        "Returning dim jenissekolah"
    );
    }

    function index() {

        function create_dim_jenissekolah($list_jk)
        {
            foreach($list_jk as $l)
            {
                $data_to_return['daftar_dim_jenissekolah'][] = array(
                    'id_dim_jenissekolah' => $l['jenissekolah_id'],
                    'jenissekolah'  => $l['jenissekolah']
                );
            }
            return $data_to_return;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}