<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model(''); //load your models here

        $this->load->library("Nusoap_lib"); //load the library here
        $this->nusoap_server = new soap_server();
        $this->nusoap_server->configureWSDL("Mahasiswa", "urn:Mahasiswa");

          /*angkatan*/
        //req angkatan array
        $this->nusoap_server->wsdl->addComplexType(
            'req_angkatan',
            'complexType',
            'array',
            'req_angkatan',
            '',
            array(
                'angkatan'       => array('name' => 'angkatan', 'type' => 'xsd:string')
            )
        );
        //list of req angkatan
        $this->nusoap_server->wsdl->addComplexType(
            'list_angkatan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'req_angkatan' => array('name' => 'req_angkatan', 'type' => 'tns:req_angkatan')),
            array(
                'req_angkatan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:req_angkatan[]')),
            'tns:req_angkatan'
        );

        //array for dim_angkatan
        $this->nusoap_server->wsdl->addComplexType(
            'dim_angkatan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'id_dim_angkatan'  => array(
                    'name'  => 'id_dim_angkatan', 'type' => 'xsd:int',
                ),
                'angkatan' => array(
                    'name' => 'angkatan', 'type' => 'xsd:int'
                )
            )
        );

        //daftar of dim angkatan
        $this->nusoap_server->wsdl->addComplexType(
            'daftar_dim_angkatan',
            'complexType', 'array', '', 'SOAP-ENC:Array',
            array(
                'dim_angkatan' => array('name' => 'dim_angkatan', 'type' => 'tns:dim_angkatan')),
            array(
                'dim_angkatan' => array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:dim_angkatan[]')),
            'tns:dim_angkatan'
        );

        //list of dim angkatan with identifier
        $this->nusoap_server->wsdl->addComplexType(
            'list_dim_angkatan',
            'complexType',
            'struct',
            'all',
            '',
            array(
                'jenis_dt' => array(
                    'name' => 'jenis_dt', 'type' => 'xsd:string'
                ),
                'daftar_dim_angkatan' => array(
                    'name' => 'daftar_dim_angkatan', 'type' => 'tns:daftar_dim_angkatan'
                )
            )
        );

        //registering create_dim_angkatan
        $this->nusoap_server->register(
            "create_dim_angkatan",
            array("list_angkatan" => "tns:list_angkatan"),
            array("return" => "tns:list_dim_angkatan"),
            "urn:Service1",
            "urn:Service1#create_dim_angkatan",
            "rpc",
            "encoded",
            "Returning dim angkatan"
        );
        /*end angkatan*/

    }

    function index() {
        /**
         * To test whether SOAP server/client is working properly
         * Just echos the input parameter
         * @param string $tmp anything as input parameter
         * @return string returns the input parameter
         */

        function create_dim_angkatan($list_angkatan)
        {
            $data_to_return = array();
            $data_to_return['jenis_dt'] = 0;
            foreach($list_angkatan as $l)
            {
                $tmp_arr = array(
                    'id_dim_angkatan' => $l['angkatan'],
                    'angkatan' => $l['angkatan']
                );
                $data_to_return['daftar_dim_angkatan'][] = $tmp_arr;
            }
            return $data_to_return;
        }

         $this->nusoap_server->service(file_get_contents("php://input")); //shows the standard info about service
        // $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        // $this->nusoap_server->service($HTTP_RAW_POST_DATA);
    }
}