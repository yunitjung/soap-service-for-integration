<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DimensiSave_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function save_dim_angkatan($data)
    {
        $this->db->insert('dim_angkatan', $data);
        return $this->db->insert_id();
    }

    function save_dim_prodi($data)
    {
        $this->db->insert('dim_prodi', $data);
        return $this->db->insert_id();
    }

    function save_dim_bk($data)
    {
        $this->db->insert('dim_bidangkompetensi', $data);
        return $this->db->insert_id();
    }

    function save_dim_jlulusan($data)
    {
        $this->db->insert('dim_jlulusan', $data);
        return $this->db->insert_id();
    }

    function save_dim_asaldaerah($data)
    {
        $this->db->insert('dim_asaldaerah', $data);
        return $this->db->insert_id();
    }

    function save_subjalur_to_db($data)
    {
        $this->db->insert('dim_subjalur', $data);
        return $this->db->insert_id();
    }

    function save_jenissekolah_to_db($data)
    {
        $this->db->insert('dim_jenissekolah', $data);
        return $this->db->insert_id();
    }

    function get_id_dim($table, $params)
    {
        $this->db->get_where($table, $params);
        return $this->db->result_array();
    }

    function get_dim($table)
    {
        return $this->db->get($table)->result_array();
    }

    function check_id($table, $params)
    {
        $this->db->select();
        $query = $this->db->get_where($table, $params);
        $result = $query->result_array();
        $count = count($result);

        return $count > 0 ? false : true;
    }

}