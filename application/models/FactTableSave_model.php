<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FactTableSave_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_angkatan($angkatan)
    {
        return $this->db->get_where('dim_angkatan', array('angkatan' => $angkatan))->row_array();
    }

    function get_semester($semester)
    {
        return $this->db->get_where('dim_semester', array('semester' => $semester))->row_array();
    }

    function get_jk($jenis_kelamin)
    {
        return $this->db->get_where('dim_jeniskelamin', array('jenis_kelamin' => $jenis_kelamin))->row_array();
    }

    function insert_ft_ips($data)
    {
        $this->db->insert('fact_ips', $data);
        return $this->db->insert_id();
    }

    function insert_ft_kelulusan($data)
    {
        $this->db->insert('fact_kelulusan', $data);
        return $this->db->insert_id();
    }

    function insert_ft_asalmhs($data)
    {
        $this->db->insert('fact_asalmhs', $data);
        return $this->db->insert_id();
    }

    function insert_ft_asalkinerja($data)
    {
        $this->db->insert('fact_asalkinerja', $data);
        return $this->db->insert_id();
    }

    function check_id($table, $params)
    {
        $this->db->select();
        $query = $this->db->get_where($table, $params);
        $result = $query->result_array();
        $count = count($result);

        return $count > 0 ? false : true;
    }

    function check_facttable($table, $params)
    {
        $this->db->select();
        $query = $this->db->get_where($table, $params);
        $result = $query->result_array();
        $count = count($result);

        return $count > 0 ? true : false;
    }
}
